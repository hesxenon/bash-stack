#!/usr/bin/env bun

import yargs from "yargs";
import * as Fs from "node:fs";
import * as Path from "node:path";
import * as Stream from "node:stream";
import * as Os from "node:os";
import * as Proc from "node:child_process";
import * as Tar from "tar";
import { task } from "fp-ts";
import { pipe } from "fp-ts/lib/function";
import chalk from "chalk";

//#region process options

const {
  template = "minimal",
  _: [destination = "."],
} = await yargs(process.argv.slice(2)).option("template", {
  alias: "t",
  choices: ["minimal", "fddd"] as const,
}).argv;

if (typeof destination !== "string") {
  console.log("Expected destination to be a string");
  process.exit(1);
}

const target = Path.join(process.cwd(), destination);
const source = Path.join(Os.tmpdir(), `bash-stack-${crypto.randomUUID()}`);

//#region task definitions

const ensureDirectoriesExist: task.Task<void> = async () => {
  if (!Fs.existsSync(source)) {
    Fs.mkdirSync(source, {
      recursive: true,
    });
  }
  if (!Fs.existsSync(target)) {
    Fs.mkdirSync(target, {
      recursive: true,
    });
  }
};

const download: task.Task<void> = async () => {
  console.log("Downloading source...");
  await fetch(
    `https://gitlab.com/hesxenon/bash-stack/-/archive/main/bash-stack-main.tar.gz`,
  )
    .then((res) =>
      !res.ok
        ? (() => {
            throw new Error("could not download template");
          })()
        : res.arrayBuffer(),
    )
    .then((buf) => {
      const stream = Stream.Readable.from(Buffer.from(buf)).pipe(
        Tar.x({
          strip: 1,
          C: source,
        }),
      );

      return new Promise<void>((resolve, reject) => {
        stream.once("finish", () => resolve());
        stream.once("error", reject);
      });
    });
};

namespace Template {
  const templateDir = Path.join(source, "templates");

  const cpSync = (from: string, to: string) =>
    Fs.cpSync(from, to, {
      recursive: Fs.statSync(from).isDirectory(),
    });

  const exec = (cmd: string) => Proc.execSync(cmd, { cwd: target });

  const updateJson = (fn: (content: any) => any) => (file: string) => {
    const path = Path.join(target, file);
    return Fs.writeFileSync(
      path,
      pipe(Fs.readFileSync(path, "utf8"), JSON.parse, fn, JSON.stringify),
    );
  };

  export const create: task.Task<void> = async () => {
    console.log(`Setting up '${chalk.magenta(template)}' template`);
    cpSync(Path.join(templateDir, template), target);
    pipe(
      "package.json",
      updateJson((json) => ({ ...json, name: Path.basename(target) })),
    );
    console.log(`Installing '${chalk.magenta(template)}' dependencies`);
    exec("pnpm install");
    console.log(`Start by running "${chalk.magenta("pnpm start")}"`);
  };
}

//#region glue tasks together and run

const run = pipe(
  ensureDirectoriesExist,
  task.flatMap(() => download),
  task.flatMap(() => Template.create),
);

run();

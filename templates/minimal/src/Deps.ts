/**
 * A handy pattern borrowed from deno to aggregate your dependencies and e.g. rename them, change their import structure and cautiously extend that functionality.
 *
 * This should NOT become another "Utils" module.
 */
// this _is_ the root that the restriction points to
/* eslint-disable no-restricted-imports */

import "htmx-tsx";

export { A } from "andale";
export * from "fp-ts";
export { flow, pipe, flip, apply, identity } from "fp-ts/lib/function.js";
export * as TSX from "tsx-to-html";
export { z as Zod } from "zod";
export * from "bun:sqlite";
export * from "ts-pattern";
import Path from "node:path";
export { Path };

//#region TINY and pure utils that imho should be included in some of the above libraries
// THIS IS NOT THE PLACE TO ADD EVERY UTIL FN YOU CAN THINK OF

export const ignore = () => {};
export const tap =
  <T>(fn: (value: T) => void) =>
  (value: T) => {
    fn(value);
    return value;
  };

import { A, TSX, flow, pipe } from "Deps";

/**
 * minimal middleware
 */
const middleware = pipe(
  // start with a reqular HTTP context
  A.Middleware.lazy<
    A.HTTP.Context & {
      shell: (children: JSX.Node) => JSX.Element;
    }
  >(),
  // add a function to convert strings to HTML responses
  A.HTTP.Middleware.html(),
  // extend the context with...
  A.Context.extend(({ request, html, shell }) => {
    const isHtmxRequest = request.headers.get("hx-request");

    const wrapInShell = isHtmxRequest
      ? (node: JSX.Node) => TSX.Fragment({ children: node })
      : shell;

    return {
      isHtmxRequest,
      response: {
        html: flow(wrapInShell, TSX.toHtml, html),
      },
    };
  }),
);

/**
 * pre-apply a middleware, learn more about this here: https://hesxenon.gitlab.io/andale/recipes/using-middlewares/#but-even-that-is-a-bit-much-for-me
 */
const handler = A.Middleware.imap(middleware);

const home = A.route("/", {
  GET: handler(({ response }) =>
    response.html(
      <h1
        hx-trigger="click"
        hx-get={A.url(helloWorld).get()}
        hx-swap="outerHTML"
        style={{ cursor: "pointer" }}
      >
        Hello World?
      </h1>,
    ),
  ),
});

const helloWorld = A.route("/hello/world", {
  GET: handler(({ response }) => response.html(<h1>Hello World!</h1>)),
});

function shell(children: JSX.Node) {
  return (
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <script src="https://unpkg.com/htmx.org@1.9.9"></script>
        <script src="https://unpkg.com/hyperscript.org@0.9.12"></script>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css"
        />
      </head>
      <body>{children}</body>
    </html>
  );
}

export const { handle } = pipe(
  A.empty,
  A.register(home),
  A.register(helloWorld),
  A.create({ shell }),
);

const server = Bun.serve({
  fetch: handle,
});
console.log(`ready to BASH at ${server.hostname}:${server.port}`);

# FDDD - Functional Domain Driven Design

A BASH template that demoes (in a tiny way) a DDD approach in a functional way.

## Suggested Architecture

The architecture, as it is, is influenced by Scott Wlaschin's book [Domain Modeling Made Functional](https://www.oreilly.com/library/view/domain-modeling-made/9781680505481/) and general functional concepts.

As such it might seem daunting at first but as long as you think of your endpoints as a sort of conveyor belt where your request is being transformed into a response you'll be just fine.

It is also layered with the flow of dependencies being roughly like this (subsequent modules can depend on previous modules):

1. Deps
   - encapsulate your dependencies
   - this is similar to what you'd do in Deno
   - it's quite useful to guide what should and shouldn't be imported
   - you can also alias or change the import structure through this
1. Web
   - the port that gives you access to the web
   - mostly used to specify basic contexts for your handlers
1. Components
   - basic UI components
1. Db
   - define your data schema
   - shared between slices
1. Slices
   - the vertical slices of your app
   - each slice can decide how to deal with its view on the data itself
   - generally they should have a domain, a repository and a web layer
1. App
   - how to create your app and defining a "shell"
1. index
   - setting up the environment and giving it to the app

Looking through the code you'll notice that there's one exception: the `Routes` modules is being imported by the `Components` module. This is so that your components can know which endpoints are available in your application, and it's more or less okay since it's only a `type` being imported, not some actual value.

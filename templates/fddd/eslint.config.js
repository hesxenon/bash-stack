/* eslint-env node */
import eslint from "@eslint/js";
// eslint-disable-next-line no-restricted-imports
import tslint from "typescript-eslint";
import Fs from "node:fs";

const pkg = JSON.parse(Fs.readFileSync("./package.json", "utf8"));

export default tslint.config(
  eslint.configs.recommended,
  ...tslint.configs.recommended,
  {
    rules: {
      "no-restricted-imports": [
        "error",
        {
          paths: [
            ...Object.keys(pkg.dependencies).map((name) => ({
              name,
              message: "please use Deps.ts instead",
            })),
            ...Object.keys(pkg.devDependencies).map((name) => ({
              name,
              message:
                "dev dependencies should not be imported in production code",
            })),
          ],
        },
      ],
      "@typescript-eslint/no-namespace": "off", // premise for this rules reasoning is broken, namespaces AREN'T modules. E.g. nested modules aren't possible.
    },
  },
);

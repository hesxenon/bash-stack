/**
 * A Todo slice
 *
 * Slices have access to all outer ports and encapsulate a single feature in its entirety.
 * They are loosely coupled via a message bus IF COUPLED AT ALL. _Within_ a slice the layers
 * should be highly cohesive though.
 *
 * You can split the namespaces of this module into dedicated files if you want to.
 */
import { Errors } from "Components";
import { schema, withDb } from "Db";
import { A, Squealy, Zod, pipe, ulid } from "Deps";
import { context } from "Web";

namespace Domain {
  export namespace Todo {
    export const schema = Zod.object({
      id: Zod.string(),
      text: Zod.string(),
      isDone: Zod.coerce.boolean(),
    });
  }
  export type Todo = Zod.infer<typeof Todo.schema>;

  export namespace TodoForm {
    export const schema = Todo.schema.omit({ id: true });
  }
  export type TodoForm = Zod.infer<typeof TodoForm.schema>;

  export namespace CreateTodoCommand {
    export const schema = Todo.schema.omit({ id: true });
  }
  export type CreateTodoCommand = Zod.infer<typeof CreateTodoCommand.schema>;
}

namespace Components {
  export function TodoForm() {
    return (
      <form
        id="new-todo"
        hx-post={A.url(Routes.todo).post()}
        hx-target="next section h2"
        hx-swap="afterend"
        _="on htmx:afterRequest reset() me"
      >
        <Errors issues={[]} />
        <label>
          What do you want to do?
          <textarea name="text" required></textarea>
        </label>
        <button type="submit">Add todo</button>
      </form>
    );
  }

  export function Todo({ id, text, isDone }: Domain.Todo) {
    return (
      <form
        id={`todo-${id}`}
        class="todo"
        hx-put={A.url(Routes.todo).put({ id })}
        hx-trigger="change"
        hx-swap="outerHTML"
      >
        <Errors issues={[]} />
        <label>
          What do you want to do?
          <textarea name="text" required>
            {text}
          </textarea>
        </label>
        <label>
          <input type="checkbox" name="isDone" checked={isDone} />
          Is it done?
        </label>
      </form>
    );
  }

  export function Overview() {
    return (
      <div>
        <TodoForm />
        <div
          hx-get={A.url(Routes.todos).get()}
          hx-trigger="load"
          hx-swap="outerHTML"
        ></div>
      </div>
    );
  }
}

namespace Repository {
  const {
    query,
    select,
    insert,
    update,
    delete_,
    where,
    orderBy,
    sql,
    get,
    run,
    all,
    returning,
  } = Squealy;

  /**
   * maps a row to a domain object.
   *
   * The reason for explicitly specifying the row type "again" here is that,
   * depending on the query that returned the row,
   * a row might look different from what is specified in the schema.
   *
   * This means that using the schema specification here would provide a sense
   * of false security that can easily lead to pitfalls later on.
   */
  const toDomain = (row: {
    id: string;
    text: string;
    doneTimestamp: string | null;
  }): Domain.Todo => ({
    id: row.id,
    text: row.text,
    isDone: row.doneTimestamp != null,
  });

  export const getTodos = () =>
    withDb(async (run) => {
      const rows = await run(
        pipe(
          query(schema),
          select("todo"),
          orderBy((todo) => sql`${todo.id} DESC`),
          all,
        ),
      );

      return rows.map(toDomain);
    });

  export const createTodo = (todoForm: Domain.CreateTodoCommand) =>
    withDb(async (exec) =>
      toDomain(
        (await pipe(
          query(schema),
          insert("todo", [
            {
              id: ulid(),
              text: todoForm.text,
              doneTimestamp: null,
            },
          ]),
          returning(),
          get,
          exec,
        ))!,
      ),
    );

  export const updateTodo = ({
    id,
    ...updates
  }: Pick<Domain.Todo, "id"> & Partial<Domain.Todo>) =>
    withDb(async (exec) => {
      return toDomain(
        (await pipe(
          query(schema),
          update("todo", updates),
          where((todo) => sql`${todo.id} = ${id}`),
          returning(),
          get,
          exec,
        ))!,
      );
    });

  export const deleteTodo = ({ id }: Pick<Domain.Todo, "id">) =>
    withDb(async (exec) =>
      pipe(
        query(schema),
        delete_("todo"),
        where((todo) => sql`${todo.id} = ${id}`),
        run,
        exec,
      ),
    );
}

namespace Routes {
  const {
    HTTP: { handle, Validate },
  } = A;

  export const overview = A.route("/todos/overview", {
    GET: pipe(
      context,
      handle(({ response }) => response.html(Components.Overview())),
    ),
  });

  export const todos = A.route("/todos", {
    GET: pipe(
      context,
      handle(async ({ withDb, response }) => {
        const todos = await withDb(Repository.getTodos());

        return response.html(todos.map(Components.Todo));
      }),
    ),
  });

  export const todo = A.route("/todo", {
    POST: pipe(
      context,
      Validate.body(Domain.CreateTodoCommand.schema),
      handle(async ({ body, withDb, response }) => {
        const created = await withDb(
          Repository.createTodo({ ...body, isDone: false }),
        );

        return response.html(Components.Todo(created));
      }),
    ),
    PUT: pipe(
      context,
      Validate.query({ id: Zod.string() }),
      Validate.body(Domain.TodoForm.schema),
      handle(async ({ withDb, response, query, body }) => {
        const updated = await withDb(
          Repository.updateTodo({ ...query, ...body }),
        );

        return response.html(Components.Todo(updated));
      }),
    ),
  });
}

export const routes = pipe(A.empty, A.register(Routes.todos));

/**
 * Web Port
 */
import { A, Squealy, TSX, apply, flow, identity, pipe } from "Deps";

/**
 * a primitive context that encapsulates functions typically needed to handle requests
 */
export const context = pipe(
  A.Middleware.lazy<
    A.HTTP.Context & {
      db: Squealy.Adapter;
      shell: (props: { children: JSX.Node }) => JSX.Element;
    }
  >(),
  A.HTTP.Middleware.html(),
  A.Context.extend(({ request, html, db, shell }) => {
    const isHtmxRequest = request.headers.get("hx-request");

    const wrapInShell = isHtmxRequest
      ? (children: JSX.Node) => TSX.Fragment({ children })
      : (children: JSX.Node) => shell({ children });

    return {
      withDb: apply(db),
      response: {
        send: identity<Response>,
        /**
         * send JSX Nodes as an HTML 200 Response
         */
        html: flow(wrapInShell, TSX.toHtml, html),
        redirect: (target: string) =>
          isHtmxRequest
            ? new Response(undefined, { headers: { "hx-redirect": target } })
            : Response.redirect(target),
      },
    };
  }),
);

/**
 * the programs entry point.
 *
 * Set up the connections to the adapters (ports & adapters, e.g. databases or incoming requests) here.
 *
 * This should be the only module with hardcoded (or process bound) values.
 */
import { Database, SquealyBunny } from "Deps";
import * as App from "App";
import * as Db from "Db";

const adapter = SquealyBunny.create(new Database("bash.sqlite"));

await Db.create(adapter);

const { handle } = App.create(adapter);

const server = Bun.serve({
  fetch: handle,
});

console.log(`listening on ${server.hostname}:${server.port}`);

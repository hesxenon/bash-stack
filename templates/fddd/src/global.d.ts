export declare global {
  namespace JSX {
    namespace HTML {
      interface HypescriptAttributes {
        _?: string;
      }
      interface IntrinsicAttributes extends HypescriptAttributes {}
    }
  }
}

/**
 * generic UI components
 */

export function Errors({ issues }: { issues: Zod.ZodIssue[] }) {
  return (
    <ul class="errors">
      {issues.map((issue) => (
        <li>{issue.message}</li>
      ))}
    </ul>
  );
}

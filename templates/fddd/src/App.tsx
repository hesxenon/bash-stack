/**
 * The application layer.
 *
 * This layer glues together the Web, Repository and Domain layers.
 */
import { A, pipe, Squealy } from "Deps";
import * as Todo from "slices/Todo";

export function Shell({ children }: { children: JSX.Node }) {
  return (
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <script src="https://unpkg.com/htmx.org@1.9.9"></script>
        <script src="https://unpkg.com/hyperscript.org@0.9.12"></script>
        <link rel="stylesheet" href="public/index.css" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css"
        />
      </head>
      <body>
        <main class="container">{children}</main>
      </body>
    </html>
  );
}

const publicRoutes = pipe(
  A.empty,
  A.register(A.Utils.staticFiles("/public/*")),
);

export const create = (adapter: Squealy.Adapter) =>
  pipe(
    A.empty,
    A.register(
      A.route("/", {
        GET: pipe(
          A.HTTP.context(),
          A.HTTP.handle(() => new Response("welcome to the bash-stack")),
        ),
      }),
    ),
    A.use(publicRoutes),
    A.use(Todo.routes),
    A.create({ shell: Shell, db: adapter }),
  );

/**
 * Db Port
 */
import { Squealy, apply } from "Deps";

export const schema = Squealy.createSchema(
  {
    todo: {
      id: ["text", "PRIMARY KEY"],
      text: ["text", "NOT NULL"],
      doneTimestamp: "text",
    },
  },
  {},
);

export const create = Squealy.setup(schema);

export const withDb =
  <T>(fn: (run: <U>(fn: (adapter: Squealy.Adapter) => U) => U) => T) =>
  (adapter: Squealy.Adapter) =>
    fn(apply(adapter));

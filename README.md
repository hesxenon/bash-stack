# bash-stack

- [*B*un](https://bun.sh/)
- [*A*ndale](https://hesxenon.gitlab.io/andale/)
- [*S*qlite](https://sqlite.org/index.html)
- [*H*TMX](https://htmx.org/)

A nice stack that allows you to 
- efficiently write HTMX
- forget about stale URLs in your code
- utilize TSX (yes, with HTMX props, ofc...)
- compose and scale your endpoints nicely

## When should I use this?

If you want to get your website/webapp online quickly and meta frameworks such as Qwik, Next, Nuxt, Remix, Sveltekit etc. bring too much hidden complexity for your liking.
After all why should you have to keep up with the latest frameworks when the web platform that those frameworks are built on already delivers 90% of the features you (probably) need out of the box and is far more stable?

## When should I not use this?

If you have specific, immovable requirements to do stuff that can't be solved without an extensive amount of client side JS. E.g. a HTML based 3D Game. No, seriously, what are you building that can't be solved with native HTML elements (maybe web components) and a bit of hyperscript on top but HAS to run in a browser?

## Getting started

```sh
mkdir a-bashing-project && cd $_
bunx create-bash-stack
```

This sets up a minimal template, you can also create a small functional DDD inspired app with `-t fddd`. Append `--help` to see all options.

## Styling

The BASH stack comes with PicoCSS out of the box, which gives you a nice bit of styling and encourages semantic html.

## Scripting

Of course, hyperscript is also available by default ;)
